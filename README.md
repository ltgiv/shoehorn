
# Shoe horn

Shoe horn is a Text User Interface (TUI) utility created in Bash script that simplifies the process for side-loading an Operating System into a Virtual Machine at a Virtual Private Server provider (which typically use KVM or XEN) via their recovery boot (which is typically a Debian-based OS, such as [Finnix](https://en.wikipedia.org/wiki/Finnix)).

Features include:

 - Hard disk modification - it's no longer necessary to remember various commands!
     - Partition
     - Format
     - Mount
     - Zero-Wipe
 - "Hybrid" disk creation - combine mounts together to create a larger disk for an ISO to be loaded onto.
 - RAM disk creation - add storage capacity when working on a hard disk and can be combined for the Hybrid disk feature.
 - Swap file management - add, remove, mount, and unmount swap files for extending memory (at the cost of speed and hard drive use).
 - ISO disk download and load, including inline download filters to decompress images as they're downloading:
     - tar
     - bzip2
     - xz
     - lzip
     - gzip
 - Bind mount loading - some utilities create a large union filesystem with RAM, this provides an additional point of storage for you to setup and to use for the Hybrid disk feature.
 - QEMU-based emulation with a VNC or SPICE interface for installing from an ISO, as well as KVM support.

Want to see just how easy it is?  Watch this video demonstration where Shoe horn is used to download an ISO and side-load it onto Digital Ocean:

## Examples

[![Side-loading an OS onto Digital Ocean](https://img.youtube.com/vi/E_hACwJGq4g/0.jpg)](https://youtu.be/E_hACwJGq4g)

## Usage

### General

```
update-ca-certificates

mkdir --parents /tmp/shoehorn

wget \
    --output-document=/tmp/shoehorn/shoehorn.bash \
    https://gitlab.com/ltgiv/shoehorn/-/raw/master/shoehorn.bash

bash /tmp/shoehorn/shoehorn.bash
```

###  Finnix

You will need to perform these steps **before** the *General use* steps above.

```
# Change root password
passwd

# Run SSH server
service ssh start

# Add Debian Archive Automatic Signing Keys:

# Debian Archive Automatic Signing Key (9/stretch)
apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 04EE7237B7D453EC
apt-key list | grep -w B7D453EC

# Debian Archive Automatic Signing Key (10/buster)
apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 648ACFD622F3D138
apt-key list | grep -w 22F3D138
```

## Contributors
 - [Louis T. Getterman IV](https://thad.getterman.org/about)

Have something to contribute to this project?  Please see the document, [How to contribute](HOW_TO_CONTRIBUTE.md).

## Links

 - Special thanks to IT-Offshore.co.uk for these excellent instructions, [Alpine Linux KVM VPS without a Custom ISO](https://it-offshore.co.uk/linux/alpine-linux/64-alpine-linux-kvm-vps-without-a-custom-iso).  This article motivated me to create this tool.

## License

```
MIT License

Copyright (c) 2021 Louis T. Getterman IV

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
```

> Written with [Typora](https://typora.io/).

