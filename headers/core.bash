#!/usr/bin/env bash

function exitCB() {

	if "${cleanExit}"; then
		clear
		echo "$title"
		info "Program cleanly exited."
		exit 0;

	else
		echo "${title}"
		info "An error occurred, and this program was unable to complete."
		exit 1

	fi

}
trap exitCB EXIT

initialSetup() {

	info "Running initial setup."

	runAptGetUpdate;

	prerequisitesPackages;

	cleanApt;

}

menuMain() {

	local hybridMenuPrompt
	local choice

	# Bind mount
	if mountpoint --quiet '/mnt/shoebind'; then
		bindMenuPrompt="Dis"
	else
		bindMenuPrompt="En"
	fi

	# Hybrid disk
	if mountpoint --quiet '/mnt/hybrid'; then
		hybridMenuPrompt="Dis"
	else
		hybridMenuPrompt="En"
	fi

	set +e
	choice=$(
		dialog \
		--clear \
		--backtitle "${title}" \
		--title "Main menu" \
		--cancel-label "Exit" \
		--menu "\nSelect an option:" \
		15 40 6 \
			1 "Hard disk" \
			2 "RAM disk" \
			3 "Bind mount : ${bindMenuPrompt}able" \
			4 "Hybrid disk : ${hybridMenuPrompt}able" \
			5 "Swap" \
			6 "Virtualization" \
			7 "Maintenance" \
			2>&1 >/dev/tty
		)
	set -e

	# Exit
	if [ -z "${choice}" ]; then
		cleanExit=true;
		return 0;
	fi

	case $choice in

		1)
			menuStorageMain;
			;;

		2)
			menuRAMMain;
			;;

		3)
			menuBindMain;
			;;

		4)
			menuHybridDisk;
			;;

		5)
			menuSwapMain;
			;;

		6)
			menuVirtualizationMain;
			;;

		7)
			menuMaintenanceMain;
			;;

	esac

	menuMain;

}

menuPromptAlert() {

	local width=${3:-40}
	local height=${4:-15}

	dialog \
		--clear \
		--backtitle "${title}" \
		--title "$1" \
		--msgbox "\n$2" \
		"$height" "$width" \
		2>&1 >/dev/tty

}

# Main function
main() {

	clear;
	echo "${title}"
	echo;

	initialSetup;

	menuMain;

}
