#!/usr/bin/env bash

# Thanks Chaim Leib Halbert - https://stackoverflow.com/a/26500519
function getUriFilename() {

	local header="$(curl --silent --head "$1" | tr -d '\r')"
	local filename="$(echo "$header" | grep -o -E 'filename=.*$')"

	if [[ -n "$filename" ]]; then
		echo "${filename#filename=}"
		return
	fi

	filename="$(echo "$header" | grep -o -E 'Location:.*$')"
	if [[ -n "$filename" ]]; then
		basename "${filename#Location\:}"
		return
	fi

	return 1

}
