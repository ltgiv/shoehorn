#!/usr/bin/env bash

unmount() {

	local waitTime="${2:-30}"
	local lazy="${3:-0}" # see https://stackoverflow.com/a/58121313
	local i=0
	local mountTableEntry

	if [ "${lazy}" == "lazy" ]; then
		umount --force --lazy "$1" >/dev/null 2>&1 || true
	else
		umount --force "$1" >/dev/null 2>&1 || true
	fi

	for ((i = 0; i < ${waitTime}; ++i)); do

		mountTableEntry=`df --all --output=source,target | egrep "$1\$"` || true

		if [ -z "${mountTableEntry}" ]; then
			return 0

		# Wait # seconds
		else
			echo "$(( 100*(++i)/${waitTime} ))" | dialog \
				--backtitle "${title}" \
				--title "Waiting for unmount" \
				--gauge "$1" \
				10 70 0 \
				2>&1 >/dev/tty
			sleep 1

		fi

	done

	return 1

}

detachLoopDevice() {

	local waitTime=30
	local loopTableEntry
	local i=0

	( losetup --detach "$1" >/dev/null 2>&1 >/dev/null ) || true

	echo 0 | dialog \
	--clear \
	--backtitle "${title}" \
	--title "Waiting for detachment" \
	--gauge "$1" \
	10 70 0 \
	2>&1 >/dev/tty

	for ((i = 0; i < ${waitTime}; ++i)); do

		loopTableEntry=`losetup --list --output NAME --noheadings | grep "$1"` || true

		if [ -z "${loopTableEntry}" ]; then
			return 0

		# Wait # seconds
		else

			echo "$(( 100*(++i)/${waitTime} ))" | \
				dialog \
				--backtitle "${title}" \
				--title "Waiting for detachment" \
				--gauge "$1" \
				10 70 0 \
				2>&1 >/dev/tty

			sleep 1

		fi

	done

	return 1

}

diskSize() {

	local col="${2:-size}"
	local unit="${3:-MB}"
	local ds=`df --all --block-size="$unit" --output="$col" "$1" 2>/dev/null | tail -n +2 | awk '{print $1}'` || true

	if [ -z "$ds" ]; then
		echo "";
		>&2 echo "$1 not found."
		return 1
	fi

	local ds="${ds//[!0-9]/}"
	echo "$ds"

}
