#!/usr/bin/env bash

# Thanks Nam Nguyen - https://askubuntu.com/a/589036
function trimString() {

	# Read-only variable
	local -r string="${1}"

	# Strip space at the beginning and end
	sed -e 's/^ *//g' -e 's/ *$//g' <<< "${string}"

}

# Thanks Nam Nguyen - https://askubuntu.com/a/589036
function isEmptyString() {

	# Read-only variable
	local -r string="${1}"

	if [[ "$(trimString "${string}")" = '' ]]; then
		echo 'true'
	else
		echo 'false'
	fi

}
