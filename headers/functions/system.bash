#!/usr/bin/env bash

# Thanks Nam Nguyen - https://askubuntu.com/a/589036
function info() {

	# Read-only variable
	local -r message="${1}"

	# Light blue color
	echo -e "\033[1;36m${message}\033[0m" 2>&1

}

# Thanks Nam Nguyen - https://askubuntu.com/a/589036
function getLastAptGetUpdate() {

	local aptDate="$(stat -c %Y '/var/cache/apt')"
	local nowDate="$(date +'%s')"

	echo $((nowDate - aptDate))

}

killProcess() {

	local waitTime=30
	local procEntry
	local i=0

	( pkill --full "$1" >/dev/null 2>&1 >/dev/null ) || true

	echo 0 | dialog \
	--clear \
	--backtitle "${title}" \
	--title "Waiting for process to end" \
	--gauge "$1" \
	10 70 0 \
	2>&1 >/dev/tty

	# Destroy : wait
	for ((i = 0; i < ${waitTime}; ++i)); do

		procEntry=`pgrep --full "$1"` || true

		if [ -z "${procEntry}" ]; then
			return 0

		# Wait # seconds
		else
			echo "$(( 100*(++i)/${waitTime} ))" | \
				dialog \
				--backtitle "${title}" \
				--title "Waiting for process to end" \
				--gauge "$1" \
				10 70 0 \
				2>&1 >/dev/tty
			sleep 1

		fi

	done

	return 1

}
