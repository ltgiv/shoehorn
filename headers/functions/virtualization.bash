#!/usr/bin/env bash

hasKVM() {

	set +e

	egrep --color=auto 'vmx|svm|0xc0f' /proc/cpuinfo >/dev/null 2>&1

	if [ "$?" -eq 0 ]; then
		echo 'true'

	else
		echo 'false'

	fi

	set -e

}
