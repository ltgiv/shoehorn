#!/usr/bin/env bash

menuBindMain() {

	if mountpoint --quiet '/mnt/shoebind'; then
		menuBindMountDisable
	else
		menuBindMountEnable
	fi

}

menuBindMountDisable() {

	# Unmount bind
	unmount /mnt/shoebind

	# Remove bind mount directory
	rm --dir /mnt/shoebind

}

menuBindMountEnable() {

	local choice

	set +e
	choice=$(
		dialog \
		--clear \
		--backtitle "${title}" \
		--title "Select (or create) a directory to bind mount to /mnt/shoebind" \
		--cancel-label "Back" \
		--dselect "/tmp/" \
		15 -1 \
			2>&1 >/dev/tty
	)
	set -e

	# Return to prior menu.
	if [ -z "${choice}" ]; then return 0; fi

	# Directory selected
	if [ -e "${choice}" ] && [ -d "${choice}" ]; then
		true

	# Create directory
	elif [ ! -e "${choice}" ]; then
		mkdir --parents --verbose "${choice}"

	# Invalid selection
	else
		menuPromptAlert "Invalid selection" "Cannot bind mount ${choice}"
		return 0

	fi

	mkdir --parents /mnt/shoebind
	mount --bind "${choice}" /mnt/shoebind

	menuPromptAlert "Bind mount : /mnt/shoebind" "Bind mount from ${choice} has been created at /mnt/shoebind with $( diskSize '/mnt/shoebind' ) MB of available space.\n\nPlease be advised: in recovery boots such as Finnix this space is free RAM, and thus you wouldn't need to create a RAM disk as well.  If you use too much RAM, you will cause a loss of stability and possibly crash your system." 79

}
