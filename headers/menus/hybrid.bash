#!/usr/bin/env bash

menuHybridDisk() {

	if mountpoint --quiet '/mnt/hybrid'; then
		menuHybridDiskDisable
	else
		menuHybridDiskEnable
	fi

}

menuHybridDiskDisable() {

	# Obtain primary loop device.  e.g. /dev/loop1
	local loopPrimary=`mount | grep "on /mnt/hybrid" | awk '{ print $1 }'`

	# Obtain shared UUID across split drives.
	local uuidPrimary="$( blkid $loopPrimary -s UUID -o value )"

	local hybridLoopDevs
	local loopDevice
	local imageFile
	local i

	# Locate all associated loop devices
	readarray -t hybridLoopDevs <<<"$( blkid -s UUID | grep "${uuidPrimary}" | awk 'sub(/:/,"",$1){ print $1 }' )"

	# Unmount hybrid disk
	unmount /mnt/hybrid

	# Remove hybrid disk mount directory
	rm --dir /mnt/hybrid

	# Iterate associated loop devices, detach them, and then remove their disk image.
	i=0
	for ((i = 0; i < ${#hybridLoopDevs[@]}; ++i)); do

		loopDevice="${hybridLoopDevs[$i]}"
		imageFile=$( losetup --noheadings --output BACK-FILE ${loopDevice} )

		detachLoopDevice "${loopDevice}";

		rm --force "${imageFile}" 2>&1 >/dev/null

		sleep 1

	done

}

menuHybridDiskEnable() {

	# Thanks: https://unix.stackexchange.com/questions/59737/virtual-file-made-out-of-smaller-ones-for-mac-like-sparse-bundle-solution

	local rc
	local choice
	local choices
	local ramDiskInfo=`df -all --block-size=M --output=source,target,size | egrep "^ramdisk"` || true
	local bindMountInfo=`df -all --block-size=M --output=source,target,size | awk '{ print $2 }' | grep /mnt/shoebind` || true
	local partChoices
	local partitionCount
	local hybridMenuChoices
	local y_offset
	local availableSize
	local i
	local partCurr
	local partDev
	local partMount
	local partSize
	local diskPass
	local devMnt
	local devAvailable
	local setSize
	local loopDevices

	readarray -t partChoices < <(lsblk --noheadings --paths --sort NAME --list --output NAME,MOUNTPOINT,SIZE,TYPE | grep part | grep '/mnt/' )

	partitionCount=0
	[[ ! -z "$ramDiskInfo" ]] && partitionCount=$(( $partitionCount + 1 )) || partitionCount=$(( $partitionCount + 0 ))
	[[ ! -z "$bindMountInfo" ]] && partitionCount=$(( $partitionCount + 1 )) || partitionCount=$(( $partitionCount + 0 ))

	partitionCount=$(( ${partitionCount} + ${#partChoices[@]} ))

	if [ "${partitionCount}" -lt 2 ]; then
		menuPromptAlert \
			"Mounts not found" \
			"A hybrid disk requires at least 2 disks to be mounted (e.g. a RAM disk and a hard disk partition, or two hard disk partitions)."
		return 0
	fi

	hybridMenuChoices=()
	y_offset=0

	# RAM Disk
	availableSize="$( diskSize /mnt/shoeram/ 'avail' )" || true
	if [ ! -z "$availableSize" ]; then
		y_offset=$(( y_offset + 1 ))
		hybridMenuChoices+=( "/mnt/shoeram" $y_offset 1 "$availableSize" $y_offset 20 6 5 0 )
	fi

	# Bind disk
	availableSize="$( diskSize /mnt/shoebind/ 'avail' )" || true
	if [ ! -z "$availableSize" ]; then
		y_offset=$(( y_offset + 1 ))
		hybridMenuChoices+=( "/mnt/shoebind" $y_offset 1 "$availableSize" $y_offset 20 6 5 0 )
	fi

	i=0
	for ((i = 0; i < ${#partChoices[@]}; ++i)); do

		partCurr="${partChoices[$i]}"

		partDev="$( echo $partCurr | awk '{ print $1 }' )"
		partMount="$( echo $partCurr | awk '{ print $2 }' )"
		partSize="$( echo $partCurr | awk '{ print $3 }' )"

		availableSize="$( diskSize $partMount 'avail' )"

		hybridMenuChoices+=( "$partMount" "$(( $i + $y_offset + 1 ))" 1 "$availableSize" "$(( $i + $y_offset + 1 ))" 20 6 5 0 )

	done

	set +e
	choices=$(
		dialog \
		--clear \
		--backtitle "${title}" \
		--title "Configure hybrid disk : /mnt/hybrid" \
		--cancel-label "Back" \
		--mixedform "\nIn Shoe horn, a hybrid disk is a virtual disk created from two or more mounted partitions.\n\nChoose how much space in megabytes from each location you want to combine together to create a hybrid disk:" \
		15 0 0 \
			"${hybridMenuChoices[@]}" \
		2>&1 >/dev/tty
	)
	rc="$?"
	set -e

	# Cancel
	if [ "$rc" -eq 1 ]; then
		return 0
	fi

	# Solve funky newline problem (come back and fix this bug?)
	readarray -t choices <<<"${choices}"

	# Sanity check
	diskPass=0

	i=0
	for ((i = 0; i < ${#choices[@]}; ++i)); do

		devMnt="${hybridMenuChoices[$(( $i * 9 ))]}"

		devAvailable="$( diskSize $devMnt 'avail' )"

		setSize="${choices[$i]}"

		# Request is larger than capability
		if [ "$setSize" -gt "$devAvailable" ]; then
			menuPromptAlert "Not enough space : $devMnt" "\nOnly $devAvailable MB of space is available, but you've attempted to use $setSize MB of space."
			menuHybridDisk
			return 0

		# Size is too small
		elif [ "${setSize}" -gt 0 ] && [ "${setSize}" -lt 16 ]; then
			menuPromptAlert "Disk size too small" "You've set ${devMnt} to ${setSize} MB, which is too small.  The minimum supported size is 16 MB.";
			menuHybridDisk
			return 0

		# Everything is good, so far...
		elif [ "${setSize}" -ge 16 ]; then
			diskPass=$(( $diskPass + 1 ))

		fi

	done

	# A single disk defeats the point
	if [ "${diskPass}" -lt 2 ]; then
		menuPromptAlert \
			"Not enough disks" \
			"A hybrid disk requires at least 2 disks to be used (e.g. a RAM disk and a hard disk partition, or two hard disk partitions)."
		menuHybridDisk
		return 0
	fi

	# Action
	i=0
	loopDevices=()
	for ((i = 0; i < ${#choices[@]}; ++i)); do

		devMnt="${hybridMenuChoices[$(( $i * 9 ))]}"

		devAvailable="$( diskSize $devMnt 'avail' )"

		setSize="${choices[$i]}"

		# Skip disk
		if [ "${setSize}" -eq 0 ]; then continue; fi

		# Create disk image
		(
			dd if=/dev/zero bs=1048576 count="$setSize" 2>/dev/null |
				pv --numeric --size "$(( 1024 * 1024 * $setSize ))" \
					| \
						dd of="${devMnt}/hybrid.shoe" 2>/dev/null
		) 2>&1 | dialog \
					--clear \
					--backtitle "${title}" \
					--title "Creating hybrid disk chunk" \
					--gauge "${devMnt}" 10 70 0 \
					2>&1 >/dev/tty

		# Setup loopback device connected to disk image
		losetup --find "${devMnt}/hybrid.shoe"
		loopDevices+=( "$( losetup --noheadings --output NAME --associated "${devMnt}/hybrid.shoe" )" )

	done

	# Format disk images and tie them together
	mkfs.btrfs --mixed --data single --force "${loopDevices[@]}" >/dev/null 2>&1

	# Mount hybrid disk
	mkdir -p /mnt/hybrid
	mount "${loopDevices}" /mnt/hybrid

	dialog \
		--clear \
		--backtitle "${title}" \
		--title "Hybrid disk successfully created!" \
		--no-cancel \
		--pause "\nThe hybrid disk has been successfully created and mounted at /mnt/hybrid with an available size of $( diskSize /mnt/hybrid avail ) MB." \
		10 70 30 \
		2>&1 >/dev/tty

}
