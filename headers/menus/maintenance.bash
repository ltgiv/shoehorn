#!/usr/bin/env bash

menuMaintenanceMain() {

	local choice

	set +e
	choice=$(
		dialog \
		--clear \
		--backtitle "${title}" \
		--title "Maintenance" \
		--cancel-label "Back" \
		--menu "\nPlease select an option:" \
		15 0 4 \
			1 "Detach loop devices" \
			2 "Purge all hybrid images" \
			3 "Purge package cache $(du --summarize --block-size=M --sum /var/lib/apt/lists/ | cut --fields=1)" \
			4 "Package management" \
			5 "Reboot system" \
			6 "Shutdown system" \
			2>&1 >/dev/tty
		)
	set -e

	# Return to prior menu.
	if [ -z "${choice}" ]; then
		true;
		return 0;
	fi

	case $choice in

		# Detach loop devices
		1)
			menuLoopDevices
			;;

		# Delete hybrid images
		2)
			menuHybridImages
			;;

		3)
			cleanApt
			;;

		4)
			clear
			aptitude
			;;

		# Reboot system
		5)
			reboot
			;;

		# Shutdown system
		6)
			poweroff || shutdown -h now
			;;

	esac

	menuMaintenanceMain;

}

menuLoopDevices() {

	local loopChoices
	local loopMenuChoices
	local i
	local loopID
	local loopImageFile
	local choices
	local rc
	local choice

	readarray -t loopChoices < <( losetup --list --output NAME,BACK-FILE --noheadings | grep hybrid.shoe )

	# No loop devices exist.
	if [ "${#loopChoices[@]}" -eq "0" ]; then
		menuPromptAlert "No loop devices." "No loop devices were found."
		return 0
	fi

	loopMenuChoices=()
	i=0
	for ((i = 0; i < ${#loopChoices[@]}; ++i)); do

		loopID=`echo "${loopChoices[$i]}" | awk '{print $1}'`
		loopID="${loopID//[!0-9]/}"

		loopImageFile=`echo "${loopChoices[$i]}" | awk '{print $2}'`

		loopMenuChoices+=( $loopID "$loopImageFile" off )

	done

	set +e
	choices=$(
		dialog \
		--clear \
		--backtitle "${title}" \
		--title "Unmount loop devices" \
		--cancel-label "Back" \
		--checklist "Select loop devices to disconnect:" \
		15 0 5 \
			"${loopMenuChoices[@]}" \
		2>&1 >/dev/tty
	)
	rc="$?"
	set -e

	# Cancel
	if [ "$rc" -eq 1 ]; then
		return 0
	fi

	# Check if choice matches
	for choice in $choices; do

		if ! detachLoopDevice "/dev/loop${choice}"; then
			menuPromptAlert "Loop device detach error" "An error was encountered when attempting to detach /dev/loop${choice}\n\nIf a volume is still mounted with this loop device, you will need to unmount it first."
			break
		fi

	done # END FOR : CHOICES

	# Nothing else left to detach
	if [ "${#choices[@]}" -eq "${#loopChoices[@]}" ]; then
		return 0

	# Loop devices still remain attached
	else
		menuLoopDevices
	fi

}

menuHybridImages() {

	rm --force /mnt/*/hybrid.shoe

}
