#!/usr/bin/env bash

menuRAMMain() {

	# Thanks: https://www.linuxbabe.com/command-line/create-ramdisk-linux

	local choice
	local rc
	local confirm

	local ramDiskSize=`df -all --block-size=M --output=source,size | egrep "^ramdisk" | awk '{print $2}'` || true
	local ramDiskSize="${ramDiskSize//[!0-9]/}"
	local ramDiskSize="${ramDiskSize:-0}"

	local memoryStats=`free -m | grep "Mem:"`
	local memTotal="$( echo $memoryStats | awk '{print $2}')"
	local memUsed="$( echo $memoryStats | awk '{print $3}')"
	local memFree="$( echo $memoryStats | awk '{print $4}')"

	local ramDiskTableEntry=`df --all --output=source,target | egrep "^ramdisk"` || true

	set +e
	choice=$(
		dialog \
			--clear \
			--backtitle "${title}" \
			--title "RAM disk : /mnt/shoeram/" \
			--ok-label "Save" \
			--cancel-label "Back" \
			--rangebox "`cat <<EOF

RAM statistics (in megabytes):
Totl : ${memTotal}
Used : ${memUsed}
Free : ${memFree}
Disk : ${ramDiskSize}

Arrow keys:
Up : reduce
Dn : increase
LR : select field

A value of zero will unmount and destroy the RAM disk.
EOF
`" \
			15 79 \
			0 \
			"${memFree}" \
			"${ramDiskSize}" \
			2>&1 >/dev/tty
	)
	rc="$?"
	choice="$( echo -e "${choice}" | tr --delete '[:space:]' )"
	set -e

	# Cancel
	if [ "$rc" -eq 1 ] || [ "${ramDiskSize}" -eq "$choice" ]; then
		return 0
	fi

	# Alert if directory exists, and a change was made.
	if [ ! -z "${ramDiskTableEntry}" ]; then

		set +e
		dialog \
			--clear \
			--backtitle "${title}" \
			--title "Danger, Will Robinson, Danger!" \
			--yesno "\nYou have made a change to the RAM disk, which will destroy all contents.\n\nProceed?" \
			15 40 \
			2>&1 >/dev/tty
		confirm="$?"
		set -e

		# Cancel
		if [ "$confirm" -eq 1 ]; then
			return 0
		fi

	fi

	unmount "/mnt/shoeram" 30 "lazy"

	# Destroy : remove mount point
	rm --recursive --force /mnt/shoeram

	# Create
	if [ "$choice" -gt 0 ]; then
		mkdir -p /mnt/shoeram
		chmod 775 /mnt/shoeram
		mount --types tmpfs --options size="${choice}m" ramdisk /mnt/shoeram
	fi

}
