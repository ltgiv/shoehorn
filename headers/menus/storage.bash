#!/usr/bin/env bash

menuStorageMain() {

	local diskChoices
	local diskMenuChoices=()
	local i=0
	local choice

	readarray -t diskChoices < <(lsblk --noheadings --paths --sort NAME --list --output NAME,TYPE,SIZE | grep disk)

	# No disks exist.
	if [ "${#diskChoices[@]}" -eq "0" ]; then

		dialog \
			--clear \
			--backtitle "${title}" \
			--title "No disks were found." \
			--msgbox "Please add 1 or more disks." \
			15 40 \
			2>&1 >/dev/tty

		return 0

	fi

	for ((i = 0; i < ${#diskChoices[@]}; ++i)); do
		diskMenuChoices+=( $i "$( echo "${diskChoices[$i]}" | awk '{print $1 " (" $3 ")"}' )" )
	done

	set +e
	choice=$(
		dialog \
		--clear \
		--backtitle "${title}" \
		--title "Configure hard disk" \
		--cancel-label "Back" \
		--menu "Select disk:" \
		15 40 4 \
			"${diskMenuChoices[@]}" \
			2>&1 >/dev/tty
		)
	set -e

	# Return to prior menu.
	if [ -z "${choice}" ]; then
		return 0

	else

		# e.g. /dev/sda
		menuStorageAction "$( echo ${diskChoices[$choice]} | awk '{print $1}' )"

	fi

}

menuStorageAction() {

	local choice
	local confirm
	local partChoice
	local rc

	set +e
	choice=$(
		dialog \
		--clear \
		--backtitle "${title}" \
		--title "Configure hard disk: ${1}" \
		--cancel-label "Back" \
		--menu "Partitioning:" \
		15 40 6 \
			1 "Mount" \
			2 "Modify table" \
			3 "Format" \
			4 "Wipe table" \
			5 "Destroy" \
			6 "Destroy all" \
			2>&1 >/dev/tty
		)
	set -e

	case $choice in

		# Mounting
		1)
			menuStorageMount "$1"
			;;

		# Modify partition table with `cfdisk`
		2)
			cfdisk $( echo $1 | awk '{print $1}' );
			menuStorageAction "$1"
			;;

		# Format partition
		3)
			menuStorageSelectPartition "$1"
			;;

		# Wipe partition table
		4)
			set +e
			dialog \
				--clear \
				--backtitle "${title}" \
				--title "Danger, Will Robinson, Danger!" \
				--yesno "\nYou are about to wipe the disk's partition table!\n\nProceed?" \
				15 40 \
				2>&1 >/dev/tty
			confirm="$?"
			set -e

			if [ "$confirm" -eq 0 ]; then

				dialog \
					--clear \
					--backtitle "${title}" \
					--title "Wiping partition table" \
					--prgbox "Disk: ${1}" \
					"wipefs --all --force '$1' 2>&1" \
					15 79 \
					2>&1 >/dev/tty

			fi

			menuStorageAction "$1"
			;;

		# Zero out partition
		5)
			set +e
			partChoice="$( menuPromptPartitionSelect "$1" "Select a partition to destroy" "Disk: $1" )"
			rc="$?"
			set -e

			# No partition
			if [ "$rc" -eq 1 ]; then
				menuPromptNoPartitions
				menuStorageAction "$1"
				return 0

			elif [ -z "$partChoice" ]; then
				menuStorageAction "$1"
				return 0
			fi

			set +e
			dialog \
				--clear \
				--backtitle "${title}" \
				--title "Danger, Will Robinson, Danger!" \
				--yesno "\nYou are about to destroy the partition's contents by replacing EVERYTHING with zeroes!\n\nProceed?" \
				15 40 \
				2>&1 >/dev/tty
			confirm="$?"
			set -e

			if [ "$confirm" -eq 0 ]; then

				(
					pv --numeric /dev/zero --size \
						`lsblk --noheadings --paths --list --bytes --output SIZE "$partChoice"` \
						| \
						dd of="$partChoice"
				) 2>&1 | dialog \
							--clear \
							--backtitle "${title}" \
							--title "Destroying partition" \
							--gauge "$partChoice" 10 70 0 \
							2>&1 >/dev/tty

			fi

			menuStorageAction "$1"

			;;

		# Zero out disk
		6)

			set +e
			dialog \
				--clear \
				--backtitle "${title}" \
				--title "Danger, Will Robinson, Danger!" \
				--yesno "\nYou are about to destroy the disk's contents by replacing EVERYTHING with zeroes!\n\nProceed?" \
				15 40 \
				2>&1 >/dev/tty
			confirm="$?"
			set -e

			if [ "$confirm" -eq 0 ]; then

				(
					pv --numeric /dev/zero --size \
						`lsblk --noheadings --paths --list --bytes --output SIZE "$1"` \
						| \
						dd of="$1"
				) 2>&1 | dialog \
							--clear \
							--backtitle "${title}" \
							--title "Destroying disk" \
							--gauge "$1" 10 70 0 \
							2>&1 >/dev/tty

			fi

			menuStorageAction "$1"
			;;

		# Back
		'')
			menuStorageMain
			;;

	esac

}

menuStorageMount() {

	local rc
	local partChoices
	local i
	local devPath
	local devMount
	local partMenuChoices=()
	local choice
	local choices
	local partitionPath
	local partitionName
	local partitionMount
	local partitionMounted
	local enabled
	local partInfo
	local partAttrs
	local partFormat

	readarray -t partChoices < <(lsblk --noheadings --paths --sort NAME --list --output NAME,TYPE,SIZE | grep ${1} | grep part)

	# No partitions exist.
	if [ "${#partChoices[@]}" -eq "0" ]; then
		menuPromptNoPartitions;
		menuStorageAction "$1";
		return 0

	fi

	i=0
	for ((i = 0; i < ${#partChoices[@]}; ++i)); do

		devPath=$( echo "${partChoices[$i]}" | awk '{print $1}' )
		devMount=`df --all --output=source,target | egrep "^${devPath}" | awk '{print $2}'` || true

		[[ ! -z "${devMount}" ]] && mounted="on" || mounted="off"
		partMenuChoices+=( $i "$( echo "${partChoices[$i]}" | awk '{print $1 " (" $3 ")"}' )" "${mounted}" )

	done

	set +e
	choices=$(
			dialog \
			--clear \
			--backtitle "${title}" \
			--title "Mounting drive partitions" \
			--cancel-label "Back" \
			--checklist "Disk: $1" \
			15 40 5 \
				"${partMenuChoices[@]}" \
			2>&1 >/dev/tty
		)
	rc="$?"
	set -e

	# Cancel
	if [ "$rc" -eq 1 ]; then
		menuStorageAction "$1";
		return 0
	fi

	# Iterate available partitions and mount/unmount where appropriate.
	i=0
	for ((i = 0; i < ${#partChoices[@]}; ++i)); do

		partitionPath=`echo "${partChoices[$i]}" | awk '{print $1}'`
		partitionName=`basename "${partitionPath}"`
		partitionMount="/mnt/${partitionName}"

		devMount=`df --all --output=source,target | egrep "^${partitionPath}" | awk '{print $2}'` || true
		[[ ! -z "${devMount}" ]] && partitionMounted=true || partitionMounted=false

		enabled=false

		# Check if choice matches
		for choice in $choices; do

			# Skip remainder of processing since current partition doesn't match choice
			if [ "$i" -ne "${choice}" ]; then
				continue
			fi

			enabled=true

		done # END FOR : CHOICES

		# Mount
		if "${enabled}"; then

			# Skip
			if "${partitionMounted}"; then continue; fi

			partInfo=`parted --machine $partitionPath print 2>/dev/null | tail -n1`
			partAttrs=(${partInfo//:/ })
			partFormat="${partAttrs[*]: -2:1}"

			# or could grep against /proc/filesystems...
			if [ -z "$( command -v mkfs.${partFormat} )" ]; then

				menuPromptAlert "Partition unformatted" "Cannot mount $partitionName since it has not been formatted."
				continue

			fi

			mkdir --parents "${partitionMount}"
			mount "${partitionPath}" "${partitionMount}"

		# Unmount
		else

			# Skip
			if ! "${partitionMounted}"; then continue; fi

			# Unmount
			set +e
			unmount "${partitionMount}"
			rc="$?"
			set -e

			# Remove empty directory
			if [ "$rc" -eq 0 ]; then
				rm --dir "${partitionMount}"
			fi

		fi

	done # END FOR : DRIVE PARTITIONS

	menuStorageMount "$1"

}

menuPromptPartitionSelect() {

	# Arguments:
	# $1 is disk
	# $2 is title
	# $3 is message

	# Results:
	# choice is 0 with rc of 1 if no partitions
	# choice is 0 with rc of 0 if back button

	local partChoices
	local partMenuChoices
	local i
	local choice

	readarray -t partChoices < <(lsblk --noheadings --paths --sort NAME --list --output NAME,TYPE,SIZE | grep ${1} | grep part)

	# No partitions exist.
	if [ "${#partChoices[@]}" -eq "0" ]; then
		echo ""
		return 1
	fi

	partMenuChoices=()
	i=0
	for ((i = 0; i < ${#partChoices[@]}; ++i)); do
		partMenuChoices+=( $i "$( echo "${partChoices[$i]}" | awk '{print $1 " (" $3 ")"}' )" )
	done

	set +e
	choice=$(
		dialog \
			--clear \
			--backtitle "${title}" \
			--title "$2" \
			--cancel-label "Back" \
			--menu "$3" \
			15 40 4 \
				"${partMenuChoices[@]}" \
				2>&1 >/dev/tty
			)
	set -e

	# Return to prior menu.
	if [ -z "${choice}" ]; then
		echo ""
		return 0

	else
		echo "${partChoices[$choice]}" | awk '{print $1}'
		return 0

	fi

}

menuStorageSelectPartition() {

	local partChoices
	local partMenuChoices
	local i
	local choice

	readarray -t partChoices < <(lsblk --noheadings --paths --sort NAME --list --output NAME,TYPE,SIZE | grep ${1} | grep part)

	# No partitions exist.
	if [ "${#partChoices[@]}" -eq "0" ]; then
		menuPromptNoPartitions;
		menuStorageAction "$1";
		return 0
	fi

	partMenuChoices=()
	i=0
	for ((i = 0; i < ${#partChoices[@]}; ++i)); do
		partMenuChoices+=( $i "$( echo "${partChoices[$i]}" | awk '{print $1 " (" $3 ")"}' )" )
	done

	set +e
	choice=$(
		dialog \
			--clear \
			--backtitle "${title}" \
			--title "Format partition" \
			--cancel-label "Back" \
			--menu "Destination:" \
			15 40 4 \
				"${partMenuChoices[@]}" \
				2>&1 >/dev/tty
			)
	set -e

	# Return to prior menu.
	if [ -z "${choice}" ]; then
		menuStorageAction "$1";

	else
		# e.g. /dev/sda, /dev/sda1
		menuStorageFormatPartition "$1" "$( echo ${partChoices[$choice]} | awk '{print $1}' )"

	fi

}

menuPromptNoPartitions() {

	menuPromptAlert "No partitions were found." "Please add 1 or more partitions with the partition table editor."

}

menuStorageFormatPartition() {

	local mkfsGlob
	local currFormat
	local formatters
	local formatMenuChoices
	local i
	local choice
	local formatChoice
	local mkfsCmd
	local confirm

	mkfsGlob=`{ IFS=:; ls -H $PATH; } | sort | grep -i mkfs.`
	formatters=()
	for currFormat in ${mkfsGlob[@]}; do
		formatters+=( "${currFormat:5}" )
	done

	formatMenuChoices=()
	i=0
	for ((i = 0; i < ${#formatters[@]}; ++i)); do
		formatMenuChoices+=( $i "${formatters[$i]}" )
	done

	set +e
	choice=$(
		dialog \
		--clear \
		--backtitle "${title}" \
		--title "Format partition : $2" \
		--cancel-label "Back" \
		--menu "Select format:" \
		15 60 15 \
			"${formatMenuChoices[@]}" \
			2>&1 >/dev/tty
		)
	set -e

	# Return to prior menu.
	if [ -z "${choice}" ]; then
		menuStorageSelectPartition "$1";
		return 0;
	fi

	formatChoice="${formatters[$choice]}"
	mkfsCmd=""

	case "$formatChoice" in

		ext2|ext3|ext4|fat|jfs|minix|msdos|reiserfs|vfat)
			mkfsCmd="yes | mkfs --type='${formatChoice}' '$2' 2>&1"
			;;

		ntfs)
			mkfsCmd="mkfs --type='${formatChoice}' --fast '$2' 2>&1"
			;;

		# TO-DO:

		# bfs)
		# 	true
		# 	;;

		# btrfs)
		# 	true
		# 	;;

		# cramfs)
		# 	true
		# 	;;

		# reiser4)
		# 	true
		# 	;;

		# reiser4)
		# 	true
		# 	;;

		# xfs)
		# 	true
		# 	;;

		*)

			dialog \
				--clear \
				--backtitle "${title}" \
				--title "Unsupported format" \
				--msgbox "At present, '${formatChoice}' is an unsupported format. If this is a format that you would like to use, you'll need to do so manually." \
				15 40 \
				2>&1 >/dev/tty

			clear
			info "You are now in a shell to format '${2}' with '${formatChoice}':"
			info "^d when you're finished."
			bash
			clear

			menuStorageSelectPartition "$1"
			return 0
			;;

	esac

	set +e
	dialog \
		--clear \
		--backtitle "${title}" \
		--title "Danger, Will Robinson, Danger!" \
		--yesno "\nYou are about to format the disk partition $2 with the '${formatters[$choice]}' format.\n\nProceed?" \
		15 40 \
		2>&1 >/dev/tty
	confirm="$?"
	set -e

	if [ "$confirm" -eq 0 ]; then

		dialog \
			--clear \
			--backtitle "${title}" \
			--title "Format partition : $2" \
			--prgbox "${formatters[$choice]}" \
			"${mkfsCmd}" \
			25 79 \
			2>&1 >/dev/tty

	fi

	menuStorageSelectPartition "$1"

}
