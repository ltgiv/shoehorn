#!/usr/bin/env bash

menuSwapMain() {

	local choice

	set +e
	choice=$(
		dialog \
		--clear \
		--backtitle "${title}" \
		--title "Swap" \
		--cancel-label "Back" \
		--menu "\nPlease select an option:" \
		15 0 4 \
			1 "Add" \
			2 "Remove" \
			2>&1 >/dev/tty
		)
	set -e

	# Return to prior menu.
	if [ -z "${choice}" ]; then
		true;
		return 0;
	fi

	case $choice in

		# Add (and mount) swap
		1)
			menuSwapAdd
			;;

		# Remove
		2)
			menuSwapRemove
			;;

	esac

	menuSwapMain;

}

menuSwapAdd() {

	local choice

	local choiceDiskInfo

	local swapDirAvail
	local swapDirType
	local swapDirMount

	# Set swap file path
	set +e
	choice=$(
		dialog \
		--clear \
		--backtitle "${title}" \
		--title "Create swap file" \
		--cancel-label "Back" \
		--fselect "/" \
		15 -1 \
			2>&1 >/dev/tty
	)
	set -e

	# Return to prior menu
	if [ -z "${choice}" ]; then return 0; fi

	# If needed, create directory
	mkdir --parents --verbose `dirname "${choice}"`

	# Resolve real path to swap file
	choice="${choice}" # Dave Dopson, Thank You! - http://stackoverflow.com/questions/59895/can-a-bash-script-tell-what-directory-its-stored-in
	while [ -h "$choice" ]; do # resolve $choice until the file is no longer a symlink
		SCRIPTPATH="$( cd -P "$( dirname "$choice" )" && pwd )"
		choice="$(readlink "$choice")"
		[[ $choice != /* ]] && choice="$SCRIPTPATH/$choice" # if $SOURCE was a relative symlink, we need to resolve it relative to the path where the symlink file was located
	done

	# Set variables that we'll use throughout this function
	choiceDiskInfo=$(df --all --block-size=M --output=size,used,avail,source,fstype,target `dirname "${choice}"` | tail -n +2)
	swapDirAvail="$( echo $choiceDiskInfo | awk '{print $3}' | tr -dc '0-9')"
	swapDirType="$( echo $choiceDiskInfo | awk '{print $5}')"
	swapDirMount=$( echo $choiceDiskInfo | awk '{print $6}')

	# Swap is already mounted
	if [ ! -z "$( swapon --show=name,size --noheadings --bytes --raw | egrep "^${choice}" | awk '{print $2}' )" ]; then
		menuPromptAlert "Swap already mounted" "You cannot add swap file '${choice}', as it's already been added."
		return 0

	# User selected a directory as their path
	elif [ -d "${choice}" ]; then
		menuPromptAlert "Invalid swap destination" "You cannot use a directory as a swap file."
		return 0

	# Invalid swap file selected
	elif [ -f "${choice}" ] && [ $( file "${choice}" | grep -c "swap file" ) -eq 0 ]; then
		menuPromptAlert "Invalid swap file" "The file that you've selected is not a valid swap file."
		return 0

	# Unusable disk type
	elif [[ "${swapDirType}" =~ ^(sysfs|proc|devtmpfs|devpts|tmpfs|iso9660|squashfs|overlay|securityfs|cgroup|cgroup2|pstore|mqueue|debugfs|hugetlbfs|fusectl|configfs|binfmt_misc)$ ]]; then
		menuPromptAlert "Invalid disk type" "You may be able to store a swap file at '${choice}' on the disk type '${swapDirType}' (mount point: ${swapDirMount}), but you won't be able to use it.\n\nPlease select a difference destination."
		return 0

	# Valid swap file selected
	elif [ -f "${choice}" ] && [ $( file "${choice}" | grep -c "swap file" ) -eq 1 ]; then
		true

	# Create swap file
	elif [ ! -e "${choice}" ]; then

		set +e
		setSwapSize=$(
			dialog \
				--clear \
				--backtitle "${title}" \
				--title "Set swap file size for ${choice}" \
				--ok-label "Save" \
				--cancel-label "Back" \
				--rangebox "`cat <<EOF

Disk statistics:
Totl : $( echo $choiceDiskInfo | awk '{print $1}')
Used : $( echo $choiceDiskInfo | awk '{print $2}')
Free : ${swapDirAvail}M
Src  : $( echo $choiceDiskInfo | awk '{print $4}')
Type : ${swapDirType}
Mnt  : ${swapDirMount}

Arrow keys:
Up : reduce
Dn : increase
LR : select field

A value of zero will cancel this operation.
EOF
`" \
				15 79 \
				0 \
				"${swapDirAvail}" \
				"0" \
				2>&1 >/dev/tty
		)
		rc="$?"
		setSwapSize="$( echo -e "${setSwapSize}" | tr --delete '[:space:]' )"
		set -e

		# Zeroed-out placeholder
		(
			dd if=/dev/zero bs=1048576 count="$setSwapSize" 2>/dev/null |
				pv --numeric --size "$(( 1024 * 1024 * $setSwapSize ))" \
					| \
						dd of="${choice}" 2>/dev/null
		) 2>&1 | dialog \
					--clear \
					--backtitle "${title}" \
					--title "Creating swap file" \
					--gauge "${choice}" 10 70 0 \
					2>&1 >/dev/tty

		# Permissions
		chmod 600 "${choice}";

		# Enable new swap file
		mkswap "${choice}";

	# Unknown failure
	else
		menuPromptAlert "Unknown failure" "An unknown failure has occurred."
		return 1

	fi

	# Enable swap
	set +e
	swapon "${choice}";
	rc="$?"
	set -e

	if [ "$rc" -eq 0 ]; then

		# local swapSizeEntry="$( swapon --show=NAME,SIZE --noheadings --raw --bytes | grep "${choice}" | awk '{print $2}' )"
		local swapSizeEntry="$( du --bytes ${choice} | cut --fields=1 | tr --delete '[:space:]' )"

		dialog \
			--clear \
			--backtitle "${title}" \
			--title "Swap file enabled : ${choice}" \
			--no-cancel \
			--pause "\nSwap memory of $(( ${swapSizeEntry} / ( 1024 * 1024 ) )) MB has been successfully added." \
			10 70 30 \
			2>&1 >/dev/tty

		unset swapSizeEntry

	else
		menuPromptAlert "Unknown failure" "An unknown failure has occurred when trying to enable swap for '${choice}'."

	fi

}

menuSwapRemove() {

	local swapChoices
	local swapMenuChoices
	local i
	local choice
	local choices
	local confirm
	local currSwapFile

	readarray -t swapChoices < <(swapon --show=type,name,size --noheadings --bytes --raw)

	swapMenuChoices=()
	i=0
	for ((i = 0; i < ${#swapChoices[@]}; ++i)); do
		swapMenuChoices+=( $i "$( echo "${swapChoices[$i]}" | awk '{print $2 " (" $1 " : " $3/(1024*1024) " MB)"}' )" "off" )
	done

	# No swap mounts
	if [ "${#swapChoices[@]}" -eq 0 ]; then
		menuPromptAlert "No swap available" "There are no swap mounts to disable."
		return 0
	fi

	set +e
	choices=$(
		dialog \
			--clear \
			--backtitle "${title}" \
			--title "Disable swap" \
			--cancel-label "Back" \
			--checklist "Swaps to disable:" \
			15 79 4 \
				"${swapMenuChoices[@]}" \
				2>&1 >/dev/tty
			)
	rc="$?"
	set -e

	# Cancel
	if [ "$rc" -eq 1 ]; then
		return 0
	fi

	# Iterate available partitions and mount/unmount where appropriate.
	i=0
	for ((i = 0; i < ${#swapChoices[@]}; ++i)); do

		currSwapFile=""

		# Check if choice matches
		for choice in $choices; do

			# Skip remainder of processing since current partition doesn't match choice
			if [ "$i" -ne "${choice}" ]; then
				continue
			fi

			currSwapFile="$( echo "${swapChoices[$i]}" | awk '{print $2}' )"

			# Disable swap
			swapoff "${currSwapFile}"

			# Delete swap file confirmation
			set +e
			dialog \
				--clear \
				--backtitle "${title}" \
				--title "Danger, Will Robinson, Danger!" \
				--yesno "\nSwap file '${currSwapFile}' has been unmounted.\n\nDo you want to keep the swap file?" \
				15 40 \
				2>&1 >/dev/tty
			confirm="$?"
			set -e

			# Delete swap file action
			if [ "${confirm}" -eq 1 ]; then
				rm -rfv "${currSwapFile}"
			fi

		done # END FOR : CHOICES

	done # END FOR : Remove swap mounts

}
