#!/usr/bin/env bash

menuVirtualizationMain() {

	local menuLoadISOToggle
	local menuQEMUtoggle
	local choice

	[[ -z "${virtISOFilePath}" ]] && menuLoadISOToggle="Mount" || menuLoadISOToggle="Eject"
	[[ -z "${virtHardDiskPath}" ]] && menuLoadDiskToggle="Mount" || menuLoadDiskToggle="Eject"
	[[ -z "$( pgrep --full "qemu-system-x86_64" )" ]] && menuQEMUtoggle="Start" || menuQEMUtoggle="Stop"

	set +e
	choice=$(
		dialog \
		--clear \
		--backtitle "${title}" \
		--title "Virtualization" \
		--cancel-label "Back" \
		--menu "\nISO: ${virtISOFilePath:-None}\nBlock device: ${virtHardDiskPath:-None}\n\nConfigure:" \
		15 0 4 \
			1 "${menuLoadISOToggle} ISO" \
			2 "${menuLoadDiskToggle} device" \
			3 "${menuQEMUtoggle}" \
			4 "Download" \
			2>&1 >/dev/tty
		)
	set -e

	# Return to prior menu.
	if [ -z "${choice}" ]; then
		true;
		return 0;
	fi

	case $choice in

		# ISO disk
		1)
			# Load
			if [ -z "$virtISOFilePath" ]; then

				mountpoint --quiet '/mnt/hybrid' && menuISOLoad "/mnt/hybrid/" || menuISOLoad "/mnt/"

			# Eject
			else
				virtISOFilePath=''

			fi
			;;

		# Hard disk
		2)
			# Load
			if [ -z "$virtHardDiskPath" ]; then
				menuVirtHardDiskLoad

			# Eject
			else
				virtHardDiskPath=''

			fi
			;;

		# Run
		3)
			menuISORun
			;;

		# Download
		4)
			menuISODownload
			;;

	esac

	menuVirtualizationMain;

}

menuISOLoad() {

	local choice

	set +e
	choice=$(
		dialog \
		--clear \
		--backtitle "${title}" \
		--title "Mount an ISO" \
		--cancel-label "Back" \
		--fselect "$1" \
		15 -1 \
			2>&1 >/dev/tty
	)
	set -e

	# Return to prior menu.
	if [ -z "${choice}" ]; then return 0; fi

	# Invalid, but with a directory
	if [ -d "${choice}" ]; then
		menuISOLoad "${choice}"
		return 0
	fi

	# File selected
	if [ -f "${choice}" ]; then
		virtISOFilePath="${choice}"
	fi

}

menuVirtHardDiskLoad() {

	local isoDiskChoices
	local treeEntries
	local i
	local currDisk
	local currTag
	local currType
	local currSize
	local currMount
	local currLevel
	local currMatch
	local choice

	readarray -t isoDiskChoices < <(lsblk --noheadings --paths --sort NAME --list --sort NAME --output NAME,TYPE,SIZE,MOUNTPOINT)

	treeEntries=()
	i=0
	for ((i = 0; i < ${#isoDiskChoices[@]}; ++i)); do

		currDisk="$( echo ${isoDiskChoices[$i]} | awk '{ print $1 }' )"
		currTag="$( basename "${currDisk}" )"
		currType="$( echo ${isoDiskChoices[$i]} | awk '{ print $2 }' )"
		currSize="$( echo ${isoDiskChoices[$i]} | awk '{ print $3 }' )"
		currMount="$( echo ${isoDiskChoices[$i]} | awk '{ print $4 }' )"
		currLevel=0
		currMatch="off"

		if [ "${currType}" = "part" ]; then
			currLevel=1
		fi

		if [ "${currDisk}" = "${virtHardDiskPath}" ]; then
			currMatch="on"
		fi

		treeEntries+=( ${currDisk} "${currDisk} (${currSize})" ${currMatch} ${currLevel} )

	done

	set +e
	choice=$(
		dialog \
			--clear \
			--backtitle "${title}" \
			--title "Mount block device" \
			--cancel-label "Back" \
			--treeview "\nSelect a block device:" \
			0 0 0 \
			"${treeEntries[@]}" \
			2>&1 >/dev/tty
	)
	set -e

	# Return to prior menu.
	if [ -z "${choice}" ]; then return 0; fi

	# Block device selected
	if [ -b "${choice}" ]; then
		virtHardDiskPath="${choice}"
	fi

}

menuISORun() {

	# Stop
	if pgrep --full "qemu-system-x86_64" > /dev/null; then
		killProcess "qemu-system-x86_64"

	# Start
	else
		menuISOStart

	fi

}

menuISOStart() {

	local qemuOptions
	local ramDiskSize

	local memoryRAMStats
	local memRAMTotal
	local memRAMUsed
	local memRAMFree

	local memorySwapStats
	local memSwapTotal
	local memSwapUsed
	local memSwapFree

	local memDflt
	local memChoice
	local protocol
	local port
	local ifAddrChoices
	local interfaces
	local i
	local ifAddr
	local bindAddress
	local rc

	# No ISO or Hard Disk are connected
	if [ -z "${virtISOFilePath}" ] && [ -z "${virtHardDiskPath}" ]; then
		menuPromptAlert "No disks were found" "An ISO and/or a Boot Device are required to start."
		return 0
	fi

	qemuOptions=(
			"-netdev type=user,id=mynet0" \
			"-device virtio-net-pci,netdev=mynet0" \
			"-rtc base=utc" \
			"-vga qxl" \
			"-daemonize"
	)

	# Boot: Hard Drive
	if [ -z "${virtISOFilePath}" ]; then
		qemuOptions+=(
			"-boot c"
		)

	# Boot: CD-ROM
	else
		qemuOptions+=(
			"-cdrom '${virtISOFilePath}'" \
			"-boot d"
		)

	fi

	# Block device
	if [ ! -z "${virtHardDiskPath}" ]; then
		qemuOptions+=(
			"-drive index=0,media=disk,if=virtio,format=raw,file='${virtHardDiskPath}'"
		)
	fi

	# Should QEMU use KVM?
	[[ "$( hasKVM )" = "true" ]] && qemuOptions+=( "-enable-kvm" ) || qemuOptions+=( "" )

	ramDiskSize=`df -all --block-size=M --output=source,size | egrep "^ramdisk" | awk '{print $2}'` || true
	ramDiskSize="${ramDiskSize//[!0-9]/}"
	ramDiskSize="${ramDiskSize:-0}"

	memoryRAMStats=`free --mebi | grep "Mem:"`
	memRAMTotal="$( echo $memoryRAMStats | awk '{print $2}')"
	memRAMUsed="$( echo $memoryRAMStats | awk '{print $3}')"
	memRAMFree="$( echo $memoryRAMStats | awk '{print $4}')"

	memorySwapStats=`free --mebi | grep "Swap:"`
	memSwapTotal="$( echo $memorySwapStats | awk '{print $2}')"
	memSwapUsed="$( echo $memorySwapStats | awk '{print $3}')"
	memSwapFree="$( echo $memorySwapStats | awk '{print $4}')"

	memDflt=0
	if [ "$(( ${memRAMFree} + ${memSwapFree} ))" -gt 512 ]; then
		memDflt=512
	
	elif [ "$(( ${memRAMFree} + ${memSwapFree} ))" -gt 256 ]; then
		memDflt=256

	fi

	# Set RAM
	set +e
	memChoice=$(
		dialog \
			--clear \
			--backtitle "${title}" \
			--title "Virtualization RAM" \
			--ok-label "Save" \
			--cancel-label "Back" \
			--rangebox "`cat <<EOF

Memory statistics (in megabytes):
RAM Free  : ${memRAMFree}
RAM Disk  : ${ramDiskSize}
Swap Free : ${memSwapFree}

Arrow keys:
Up : reduce
Dn : increase
LR : select field

Please ensure that you leave enough memory for your host OS to use.
EOF
`" \
			0 0 \
			0 \
			"$(( ${memRAMFree} + ${memSwapFree} ))" \
			"${memDflt}" \
			2>&1 >/dev/tty
	)
	rc="$?"
	set -e

	# Cancel
	if [ "$rc" -eq 1 ]; then
		return 0
	fi

	# RAM
	qemuOptions+=( "-m ${memChoice}" )

	# VNC or SPICE
	set +e
	protocol=$(
		dialog \
			--clear \
			--backtitle "${title}" \
			--title "Connection protocol" \
			--cancel-label "Back" \
			--radiolist "\nPlease select which protocol that you'd like to connect with:" \
			15 40 2 \
			v VNC on \
			s SPICE off \
			2>&1 >/dev/tty
	)
	rc="$?"
	set -e

	# Cancel
	if [ "$rc" -eq 1 ]; then
		return 0
	fi

	# Port
	set +e
	port=$(
		dialog \
			--clear \
			--backtitle "${title}" \
			--title "Port" \
			--cancel-label "Back" \
			--inputbox "\nPlease specify a port number to listen to:" \
			15 40 5900 \
			2>&1 >/dev/tty
	)
	rc="$?"
	set -e

	# Cancel
	if [ "$rc" -eq 1 ]; then
		return 0
	fi

	if ! (( ${port} >= 5900 && ${port} <= 6000 )); then
		menuPromptAlert "Invalid port : ${port}" "Please use a port number between 5900 and 6000."
		return 0
	fi

	readarray -t interfaces < <( ls /sys/class/net )

	ifAddrChoices=( 127.0.0.1 localhost on )
	ifAddrChoices+=( 0.0.0.0 'all' off )

	i=0
	for ((i = 0; i < ${#interfaces[@]}; ++i)); do

		echo "${interfaces[$i]}"

		# Turn off error abort
		set +e

		ifAddr="$( ip -f inet addr show "${interfaces[$i]}" | grep -o "inet [0-9]*\.[0-9]*\.[0-9]*\.[0-9]*" | grep -o "[0-9]*\.[0-9]*\.[0-9]*\.[0-9]*" )"

		# No matches found
		if [ "$?" -ne 0 ]; then continue; fi

		# Turn on error abort
		set -e

		# Skip loopback address
		if [ "${ifAddr}" = "127.0.0.1" ]; then continue; fi

		ifAddrChoices+=( "$( trimString "${ifAddr}" )" "$( trimString "${interfaces[$i]}" )" off )

	done

	set +e
	bindAddress=$(
		dialog \
			--clear \
			--backtitle "${title}" \
			--title "Bind to address" \
			--cancel-label "Back" \
			--radiolist "\nPlease select an address to bind to.\n\nIn most cases, you'll want to leave it set to localhost." \
			15 40 0 \
			"${ifAddrChoices[@]}" \
			2>&1 >/dev/tty
	)
	rc="$?"
	set -e

	# Cancel
	if [ "$rc" -eq 1 ]; then
		return 0
	fi

	# SPICE
	if [ "${protocol}" = "s" ]; then
		qemuOptions+=( "spice port=${port},addr=${bindAddress},disable-ticketing" )

	# VNC
	else
		qemuOptions+=( "-vnc ${bindAddress}:$(( ${port} - 5900 ))" )

	fi

	eval qemu-system-x86_64 "${qemuOptions[@]}"

}

menuISODownload() {

	local isoURL
	local rc
	local isoHeader
	local isoSize
	local choice
	local downloadFilter
	local downloadFilterName
	local isoSave
	local currPct

	set +e
	isoURL=$(
		dialog \
			--clear \
			--backtitle "${title}" \
			--title "Download URL" \
			--cancel-label "Back" \
			--inputbox "\nPlease enter an HTTP(S) URL:" \
			15 -1 \
			"" \
			2>&1 >/dev/tty
	)
	rc=$?
	set -e

	# Cancel
	if [ "$rc" -eq 1 ]; then
		return 0
	fi

	isoHeader=`curl --silent --head "${isoURL}"`
	isoSize=`echo "${isoHeader}" | grep -i Content-Length | awk '{print $2}' | tr -cd '[[:alnum:]]._-'`

	# Probably 404, but it could be that the server doesn't advise content-length.
	if [ -z "$isoSize" ]; then
		menuPromptAlert \
			"File not found" \
			"\nUnable to locate remote file at:\n\n${isoURL}\n\nPlease check your URL and try again." \
			0 15
		return 0
	fi

	# Download filters
	set +e
	choice=$(
		dialog \
		--clear \
		--backtitle "${title}" \
		--title "Main menu" \
		--cancel-label "Back" \
		--radiolist "\nIn order to reduce the amount of space needed, several file formats can be processed in transit.  If the remote file is example.iso.tgz then the 'tar with gzip' option would allow you to save the file as example.iso" \
		20 79 15 \
			0 "None" on \
			\
			1 "tar" off \
			2 "tar with bzip2" off \
			3 "tar with xz" off \
			4 "tar with lzip" off \
			5 "tar with gzip" off \
			\
			6 "bzip2" off \
			7 "xz" off \
			8 "lzip" off \
			9 "gzip" off \
			\
			2>&1 >/dev/tty
		)
	rc="$?"
	set -e

	# Back
	if [ -z "${choice}" ]; then
		return 0;
	fi

	downloadFilter=''

	case $choice in

		# None
		0)
			downloadFilter='cat'
			downloadFilterName='None'
			;;

		1)
			downloadFilter='tar'
			downloadFilterName="${downloadFilter}"
			;;

		2)
			downloadFilterName='tar with bzip2'
			downloadFilter='tar --bzip2'
			;;

		3)
			downloadFilterName='tar with xz'
			downloadFilter='tar --xz'
			;;

		4)
			downloadFilterName='tar with lzip'
			downloadFilter='tar --lzip'
			;;

		5)
			downloadFilterName='tar with gzip'
			downloadFilter='tar --gzip'
			;;

		6)
			downloadFilterName='bzip2'
			downloadFilter='bzcat'
			;;

		7)
			downloadFilterName='xz'
			downloadFilter='xz'
			;;

		8)
			downloadFilterName='lzip'
			downloadFilter='lunzip'
			;;

		9)
			downloadFilterName='gzip'
			downloadFilter='gunzip --stdout'
			;;

	esac

	# Save dialog
	set +e
	mountpoint --quiet '/mnt/hybrid' && isoSave="/mnt/hybrid/" || isoSave="/mnt/"
	isoSave="$( menuISOSave "${isoSave}" )"
	rc=$?
	set -e

	# Cancelled
	if [ "$rc" -eq 1 ]; then
		return 0
	fi

	rm -f "${isoSave}"

	clear
	currPct=0
	while IFS= read -r line; do

		currPct="${line}"
		echo "${currPct}" | \
			dialog \
				--backtitle "${title}" \
				--title "Downloading" \
				--gauge "\nFrom: ${isoURL}\nTo: ${isoSave}\nFilter: ${downloadFilterName}" \
				15 79 0 \
				;
				2>&1 >/dev/tty

	done < <( ( curl --silent "${isoURL}" | pv --numeric --size ${isoSize} | ${downloadFilter} > "${isoSave}" ) 2>&1 )

	if [ "${currPct}" -eq 100 ]; then

		# Load ISO
		virtISOFilePath="${isoSave}"

	else

		rm -f "${isoSave}"
		menuPromptAlert "Download failed at ${currPct}%" "An error occurred while downloading ${isoURL}" 79 10

	fi

}

menuISOSave() {

	local choice

	set +e
	choice=$(
		dialog \
		--clear \
		--backtitle "${title}" \
		--title "Save as" \
		--fselect "$1" \
		15 -1 \
			2>&1 >/dev/tty
	)
	set -e

	# Return to prior menu.
	if [ -z "${choice}" ]; then return 1; fi

	# Invalid, but with a directory
	if [ -d "${choice}" ]; then
		menuISOSave "${choice}"
		return 0
	fi

	echo "$choice"

}
