#!/usr/bin/env bash

# Thanks Nam Nguyen - https://askubuntu.com/a/589036
function runAptGetUpdate() {

	local updateInterval="${1}"
	local lastAptGetUpdate="$(getLastAptGetUpdate)"

	if [[ "$(isEmptyString "${updateInterval}")" = 'true' ]]; then

		# Default To 24 hours
		updateInterval="$((24 * 60 * 60))"

	fi

	if [[ "${lastAptGetUpdate}" -gt "${updateInterval}" ]]; then

		info "Update Certificate Authority certificates."
		update-ca-certificates

		info "Updating repository list."
		apt-get update -m

	else

		local lastUpdate="$(date -u -d @"${lastAptGetUpdate}" +'%-Hh %-Mm %-Ss')"
		info "Skipping repository update because its last run was '${lastUpdate}' ago."

	fi

}

prerequisitesPackages() {

	set +e

	local currPkg
	local packageList=()
	local cpl

	for currPkg in \
		'aptitude' \
		'dialog' \
		'mount' \
		'parted' \
		'pv' \
		'qemu-kvm' \
		; do

		if [ $(dpkg-query -W -f='${Status}' "${currPkg}" 2>/dev/null | grep -c "ok installed") -eq 0 ]; then
			packageList+=( "${currPkg}" )
		else
			true
		fi

	done

	# Install
	if [ ${#packageList[@]} -eq 0 ]; then

		info "All dependencies resolved."

	else

		for cpl in "${packageList[@]}"; do

			info "Installing dependency: ${cpl}..."

			apt-get install --no-install-recommends --yes --show-progress "${cpl}" >/dev/null 2>&1

			if [ "$?" -eq 0 ]; then
				info "Installing dependency: ${cpl}... Done."
			else
				info "Installing dependency: ${cpl}... Failed!"
				set -e
				false
			fi

		done

	fi

	set -e

}

function cleanApt() {

	apt-get --yes clean

	apt-get --yes autoremove

	rm -rfv /var/lib/apt/lists/*

}
