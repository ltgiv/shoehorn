#!/usr/bin/env bash
: <<'!COMMENT'

Shoe horn
https://gitlab.com/ltgiv/shoehorn

Louis T. Getterman IV
https://thad.getterman.org/about

Thanks:

Alpine Linux KVM VPS without a Custom ISO
https://it-offshore.co.uk/linux/alpine-linux/64-alpine-linux-kvm-vps-without-a-custom-iso

!COMMENT

# Abort if error encountered
set -e

# Determine core paths
################################################################################
SOURCE="${BASH_SOURCE[0]}" # Dave Dopson, Thank You! - http://stackoverflow.com/questions/59895/can-a-bash-script-tell-what-directory-its-stored-in
while [ -h "$SOURCE" ]; do # resolve $SOURCE until the file is no longer a symlink
  SCRIPTPATH="$( cd -P "$( dirname "$SOURCE" )" && pwd )"
  SOURCE="$(readlink "$SOURCE")"
  [[ $SOURCE != /* ]] && SOURCE="$SCRIPTPATH/$SOURCE" # if $SOURCE was a relative symlink, we need to resolve it relative to the path where the symlink file was located
done
################################################################################
SCRIPTPATH="$( cd -P "$( dirname "$SOURCE" )" && pwd )"
SCRIPTNAME=`basename "$SOURCE"`
################################################################################

# Global variables
version="1.1"
title="Shoe horn ${version} by Louis T. Getterman IV (Thad.Getterman.org)"

cleanExit=false		# Used for monitoring clean exit or not, used in exitCB()
virtISOFilePath=""	# Virtualization : ISO file
virtHardDiskPath="" # Virtualization : Block device

# Includes
mkdir --parents --verbose \
	"${SCRIPTPATH}/headers/functions" \
	"${SCRIPTPATH}/headers/menus" \
	;

for currHeader in \
	\
	'headers/functions/string.bash' \
	'headers/functions/system.bash' \
	'headers/functions/comm.bash' \
	'headers/functions/disk.bash' \
	'headers/functions/virtualization.bash' \
	\
	'headers/requirements.bash' \
	'headers/core.bash' \
	\
	'headers/menus/storage.bash' \
	'headers/menus/ram.bash' \
	'headers/menus/bind.bash' \
	'headers/menus/hybrid.bash' \
	'headers/menus/swap.bash' \
	'headers/menus/virtualization.bash' \
	'headers/menus/maintenance.bash' \
	\
	; do

	# Download if non-existent
	if [ ! -f "${SCRIPTPATH}/${currHeader}" ]; then
		wget \
			--output-document="${SCRIPTPATH}/${currHeader}" \
			"https://gitlab.com/ltgiv/shoehorn/-/raw/master/${currHeader}"
	fi

	source "${SCRIPTPATH}/${currHeader}"

done
unset currHeader

# Main program
main;
